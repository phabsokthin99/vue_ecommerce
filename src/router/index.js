import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import AllProductView from '@/views/AllProductView.vue'
import ProductView from '@/views/ProductView'
import PromotionView from '@/views/PromotionView'
import BlogView from '@/views/BlogView'
const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: "/allproduct",
    name: "allproduct",
    component: AllProductView
  },
  {
    path: "/product",
    name: "product",
    component: ProductView
  },
  {
    path: "/promotion",
    name: "promotion",
    component: PromotionView
  },{
    path: "/blog",
    name: "blog",
    component: BlogView
  }

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
